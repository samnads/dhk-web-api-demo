<?php
// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

////-----post-----////
//API for signup
Route::post('sign-up', 'LoginController@signUp');
Route::post('delete-account', 'LoginController@deleteAccount');
//API for save customer address details
Route::post('save-address', 'LoginController@saveAddress');
//API for sign in
Route::post('sign-in', 'LoginController@signIn');
//save booking details step1
Route::post('save-booking-one','ServiceController@saveBookingOne');
//save booking details step2
Route::post('save-booking-two','ServiceController@saveBookingTwo');
//API for forgot password
Route::post('forgot-password','LoginController@forgotPassword');
//API to check coupon code is valid or not
Route::post('check-coupon','CouponController@checkCoupon');
//API to check otp is valid or not
Route::post('check-otp','LoginController@checkOTP');
//Function to resend otp
Route::post('resend-otp','LoginController@resendOtp');
//get address of the user
Route::post('get-address','LoginController@getAddress');
//get the details in second step
Route::post('get-bookingtwo','ServiceController@getBookingTwo');
//for step1 redirection
Route::post('get-booking-one','ServiceController@getBookingOne');
//for save payment details
Route::post('make-payment','PaymentController@makePayment');
//for save package subscription payment details
Route::post('make-package-payment', 'PackagePaymentController@makePayment');
//api to calculate amount
Route::post('calculate-total-price','PaymentController@calculateTotalPrice');
//api to calculate amount
Route::post('time-availability','PaymentController@timeAvailability');
Route::post('payment-success','PaymentController@paymentSuccess');
Route::post('payment-failed','PaymentController@paymentFailed');
Route::post('cash-payment-success','PaymentController@cashPaymentSuccess');
Route::post('package-cash-payment-success', 'PackagePaymentController@cashPaymentSuccess');
Route::post('get-user-data','LoginController@getUserData');
Route::post('get-customer-odoo','LoginController@getCustomerOdooData');
Route::post('save-online-pay','PaymentController@saveOnlinePay');
Route::post('save-invoice-pay','InvoiceController@saveInvoicePay');
Route::post('online-payment-success','PaymentController@onlinePaymentSuccess');
Route::post('online-payment-failed','PaymentController@onlinePaymentFailed');
Route::post('online-package-payment-failed','PackagePaymentController@onlinePaymentFailed');
Route::post('online-package-payment-success','PackagePaymentController@onlinePaymentSuccess');
Route::post('change-package-payment-status','PackagePaymentController@changePaymentStatus');
Route::get('get-package-sub-data/{subId}', 'SubscriptionPackageSubscriptionController@getSubscriptionData');
Route::get('get-package-sub-data-new/{subId}', 'SubscriptionPackageSubscriptionController@getSubscriptionDataNew');
Route::post('check-package-payment-status','PackagePaymentController@checkPaymentStatus');
Route::post('invoice-payment-success','InvoiceController@onlinePaymentSuccess');
Route::post('invoice-payment-failed','InvoiceController@onlinePaymentFailed');
Route::post('edit-user-data','LoginController@editUserData');
Route::post('get-user-address','LoginController@getUserAddress');
Route::post('edit-address-data','LoginController@editAddressData');
Route::post('save-address-data','LoginController@saveAddressData');
Route::post('add-address-data','LoginController@addAddressData');
Route::post('delete-address','LoginController@deleteAddress');
Route::post('set-default','LoginController@setDefault');
Route::post('update-password','LoginController@updatePassword');
Route::post('view-bookings','ServiceController@viewBookings');
Route::post('save-bookinglist-pay','PaymentController@saveBookinglistPay');
Route::post('cancel-booking','PaymentController@cancelBooking');
// Route::post('check-carpet-coupon','CouponController@checkCarpetCoupon');
////-----end post-----////

////-----get-----////
//API for getting all services by passing service and coupon
Route::get('get-services/{service}/{coupon}','ServiceController@getServices');
//API for getting all services by passing service
Route::get('get-services/{service}','ServiceController@getServices');
//API for getting all services
Route::post('get-services','ServiceController@getServices');
Route::post('load-service-by-id','ServiceController@loadServiceById');
//API for get disinfection service
Route::post('get-disinfection','ServiceController@getDisinfection');
//API for deep cleaning
Route::post('get-deep-cleaning','ServiceController@getDeepCleaning');
//API for get carprt cleaning
Route::post('get-carpet-cleaning','ServiceController@getCarpetCleaning');
Route::post('get-mattress-cleaning','ServiceController@getMattressCleaning');
Route::post('get-sofa-cleaning','ServiceController@getSofaCleaning');
Route::get('hextobin/{id}','PaymentController@hextobin');
Route::get('encrypt/{id}/{id2}','PaymentController@encrypt');
Route::post('submit-rate','RatingController@submitRate');
Route::post('get-rate-review','RatingController@getRateReview');
Route::post('get-floor-scrubbing','ServiceController@getFloorScrubbing');

// API for token authentication
Route::post('token-auth','LoginController@token_auth');
// API for user_id authentication
Route::post('user-id','LoginController@user_id');

Route::post('booking-history','ServiceController@booking_history');
Route::get('faq','StaticController@faqs');
Route::get('areas','ServiceController@getServicesAreas');
Route::get('get-cleaning-services','ServiceController@getCleaningServices');
Route::post('notifications','ServiceController@notifications');
Route::post('get-userdetails','LoginController@getuserdetails');
Route::post('forgot-password-mobile','LoginController@forgotpasswordmobile');
Route::post('reset-password','LoginController@resetpassword');
Route::post('job-detail','ServiceController@jobdetails');
Route::post('get-painting','ServiceController@getPainting');
Route::post('submit-rate','RatingController@submitRate');
Route::post('get-rate-review','RatingController@getRateReview');
Route::get('test-email','LoginController@testingemail');

Route::post('booking-detail','ServiceController@booking_detail');
Route::post('update-tamara-payment','PaymentController@updateTamaraPayment');
Route::post('update-tamara-package-payment','PackagePaymentController@updateTamaraPackagePayment');
Route::post('payment-tamara-success','PaymentController@paymentTamaraSuccess');
Route::post('payment-package-tamara-success','PackagePaymentController@paymentTamaraSuccess');

Route::post('tamara-payment-failed','PaymentController@tamaraPaymentFailed');
Route::post('tamara-hook-success','PaymentController@tamaraHookSuccess');


Route::get('get-offers','LoginController@getOffers');



Route::post('cancel-booking-new','PaymentController@cancelBookingNew');


Route::get('offer-banners','LoginController@getOfferBanners');

Route::post('update-customer-avatar','LoginController@updateCustomerAvatar');

/***************************************************************************** */
// API for subscription package and their subscriptions
/* DON'T Change the order within this api
/***************************************** */
// get all packages
Route::get('packages', 'SubscriptionPackageController@getPackages');
Route::get('packages-web', 'SubscriptionPackageController@getPackagesWeb');
// get all subscriptions by package id
Route::get('packages/{id}/subscriptions', 'SubscriptionPackageSubscriptionController@getSubscriptionsByPackageId');
// get all subscriptions
Route::get('packages/subscriptions', 'SubscriptionPackageSubscriptionController@getSubscriptions');
// get specified subscriptions
Route::get('packages/subscriptions/{id}', 'SubscriptionPackageSubscriptionController@getSubscriptionById');
// get specified package
Route::get('packages/{id}', 'SubscriptionPackageController@getPackageById');
//
Route::post('package/make-payment', 'PackagePaymentController@makePaymentForPackage');
//
Route::get('package/payment/{reference_id}', 'PackagePaymentController@getPaymentDetailsByRefId');
//
Route::get('package/payment/status/{payment_status}/{reference_id}','PackagePaymentController@checkPaymentStatusByStatus');
/***************************************************************************** */
Route::get('get_cleaning_supplies/{type}', 'ConfigCleaningSuppliesController@get_cleaning_supplies_by_type');

Route::post('logout', 'LoginController@logout');

Route::post('booking-mail-status','BookingsController@updateBookingMailStatus');
Route::post('package-mail-status','SubscriptionPackageSubscriptionController@updatePackageMailStatus');


Route::get('test', function () {
    echo 'Working';
});






Route::get('test-sms','LoginController@test_sms');
Route::post('apple-process-pay','ServiceController@appleProcessPay');
Route::post('gpay-process-pay','ServiceController@gpayProcessPay');


Route::post('contact-us','ContactUsController@contact_form_send');
Route::get('my-account-data','LoginController@myAccount');

Route::get('test-mail', function(){
    $email='karthika.prasad@azinova.info';
    $name='Pradeesh J';
    $password='test@1234';
    \Illuminate\Support\Facades\Mail::send(new \App\Mail\RegistrationSuccessMail($email,$name,$password));
});



////-----end get-----////
Route::get('/', function (Request $request) {
    return 'You\'re in customer app API base url 😀 !';
});
Route::fallback(function () {
    return 'Oops, requested api endpoint not found 🥶 !';
});