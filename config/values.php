<?php

return [
'vat_amount' => env('VAT_AMOUNT'),
'cleaning_amount' => env('CLEANING_AMOUNT'),
'to_mail' => env('MAIL_TO'),
'mail_admin' => env('MAIL_ADMIN'),
'house_cleaning' => env('HOUSE_CLEANING'),
'disinfection' => env('DISINFECTION'),
'deep_cleaning' => env('DEEP_CLEANING'),
'carpet' => env('CARPET_CLEANING'),
'sofa' => env('SOFA_CLEANING'),
'mattress' => env('MATTRESS_CLEANING'),
'web_url'=>env('WEB_URL'),
'pay_by_cash_charge'=>env('PAY_BY_CASH_CHARGE'),
];
