<?php
return [
    'customer_image_upload_path' => env('customer_image_upload_path'),
    'customer_image_prefix' => env('customer_image_prefix'),
    'package_promotion_image_prefix' => env('package_promotion_image_prefix'),
    'webview_base' => env('webview_base_url'),
];
