<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerNotifications extends Model
{
    protected $table = 'customer_notifications';
    protected $primaryKey = 'id';
    public $timestamps = true;
}
