<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookMapping extends Model
{
    protected $table ='bookingservicemapping';
    public $timestamps = false;
    protected $primaryKey = 'id';

}
