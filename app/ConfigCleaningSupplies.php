<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigCleaningSupplies extends Model
{
    protected $table = 'config_cleaning_supplies';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
