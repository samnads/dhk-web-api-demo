<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerSupport extends Model
{
    protected $table = 'customer_support';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
