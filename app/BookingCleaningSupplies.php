<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCleaningSupplies extends Model
{
    protected $table = 'booking_cleaning_supplies';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
