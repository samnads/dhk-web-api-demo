<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoryCost extends Model
{
    protected $table = 'servicecategorycosts';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
