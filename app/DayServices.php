<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayServices extends Model
{
    protected $table = 'day_services';
    public $timestamps = false;
    protected $primaryKey = 'day_service_id';
}
