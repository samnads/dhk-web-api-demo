<?php

namespace App\Http\Controllers;

use App\Bookings;
use Illuminate\Http\Request;
use Response;

class BookingsController extends Controller
{
    public function updateBookingMailStatus(Request $request)
    {
        // for saving mail send or not
        // helpful for stop sending duplicate mails
        $booking = Bookings::find($request['booking_id']);
        if ($request['mail_booking_confirmation_to_customer_status'] != null) {
            $booking->mail_booking_confirmation_to_customer_status = $request['mail_booking_confirmation_to_customer_status'];
        }
        if ($request['mail_booking_confirmation_to_admin_status'] != null) {
            $booking->mail_booking_confirmation_to_admin_status = $request['mail_booking_confirmation_to_admin_status'];
        }
        $booking->save();
        return response()->json([
            'status' => "success",
            'message' => 'Booking mail(s) send status updated!',
        ], 200, array(), JSON_PRETTY_PRINT);
    }
}
