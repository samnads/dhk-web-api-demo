<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DayServices;
use App\Bookings;
class RatingController extends Controller
{
   public function submitRate(Request $request)
   {
        $date = $request['date'];
        $day_service_id = $request['day_service_id'];
        $booking_id = $request['booking_id'];
        $ratingValue = $request['ratingValue']?:'0';
        $review = $request['review'];
        $dayServices = DayServices::where('day_service_id',$day_service_id)->where('booking_id',$booking_id)->where('service_date',$date)->first();
        if($dayServices) {
            $dayServices->rating = $ratingValue;
            $dayServices->comments = $review;
            $dayServices->rate_added_date = date("Y-m-d");
            $dayServices->save();
            $bookings = Bookings::select('time_from','time_to')->find($booking_id);
            $customerName = $dayServices->customer_name;
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Rate and review submitted successfully.',
                    'data' => ['bookings'=>$bookings,'customerName'=>$customerName],
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Rate and review submission failed.',
                    'data' => $dayServices,
                ],
                200
            );
        } 
   }
   public function getRateReview(Request $request)
   {
        $date = $request['date'];
        $day_service_id = $request['day_service_id'];
        $booking_id = $request['booking_id'];
        $dayServices = DayServices::where('day_service_id',$day_service_id)->where('booking_id',$booking_id)->where('service_date',$date)->first();
        if($dayServices &&  $dayServices->rating != 0) {  
            $rateAndReview = ['rating'=>$dayServices->rating,'comments'=>$dayServices->comments];
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Rate and review fetched.',
                    'data' => $rateAndReview,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Rate and review not got.',
                    'data' => [],
                ],
                200
            );
        } 
    }
}
