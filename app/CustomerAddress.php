<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'customer_addresses';
    public $timestamps = false;
    protected $primaryKey = 'customer_address_id';
    public function getArea(){
		return $this->hasOne('App\Area','area_id','area_id');
	}
}
