<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCleaningSupplies extends Model
{
    protected $table = 'service_cleaning_supplies';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
