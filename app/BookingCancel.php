<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCancel extends Model
{
    protected $table = 'booking_cancel';
    public $timestamps = false;
    protected $primaryKey = 'booking_cancel_id';
}
