<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCode extends Model
{
    protected $table = 'coupon_code';
    public $timestamps = false;
    protected $primaryKey = 'coupon_id';
}
