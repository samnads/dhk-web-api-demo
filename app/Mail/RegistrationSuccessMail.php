<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class RegistrationSuccessMail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $name;
    public $password; 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$name,$password)
    {
        $this->email = $email;
        $this->name = $name;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    // Config::get('values.to_mail')
    public function build()
    {
        $mailSubject = 'Registration Confirmation.';
        return $this->from(Config::get('values.to_mail'), 'Dubai Housekeeping')
            ->to($this->email,$this->name)
            ->subject($mailSubject)
            ->view('emails.registration_success');  
    }
}
