<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPackagePayment extends Model
{
     protected $table = 'package_subscription_payment';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
